abstract class IDay
{
    public abstract string getInput();
    public abstract string part1();
    public abstract string part2();
    public string solve(){
        return this.GetType() + "\nPart1: " + part1() + "\nPart2: " + part2() + "\n\n";
    }
}